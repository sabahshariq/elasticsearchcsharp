﻿using System;
using Nest;
using System.Windows.Forms;

namespace elasticsearchcsharp
{
    public partial class Form1 : Form
    {
        ConnectionSettings connectionSettings;
        ElasticClient elasticClient;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            connectionSettings = new ConnectionSettings(new Uri("http://localhost:9200/")); //local PC
            elasticClient = new ElasticClient(connectionSettings);
        } 

        private void tbxSearch_TextChanged(object sender, EventArgs e)
        {
            var response = elasticClient.Search<disney>(s => s
                .Index("disney")
                .Type("character")
                .Query(q => q.QueryString(qs => qs.Query(tbxSearch.Text + "*"))));

            AutoCompleteStringCollection collection = new AutoCompleteStringCollection();
            foreach (var hit in response.Hits)
            {
                collection.Add(hit.Source.name);
            }

            tbxSearch.AutoCompleteSource = AutoCompleteSource.CustomSource;
            tbxSearch.AutoCompleteMode = AutoCompleteMode.Suggest;
            tbxSearch.AutoCompleteCustomSource = collection;

            if (tbxSearch.Text == "")
            {
                richTextBox1.Clear();
            }
        }

        private void tbxSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\b')
            {
                richTextBox1.Clear();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var response = elasticClient.Search<disney>(s => s
                .Index("disney")
                .Type("character")
                .Query(q => q.QueryString(qs => qs.Query(tbxSearch.Text + "*"))));

            if (richTextBox1.Text != " ")
            {
                richTextBox1.Clear();

                foreach (var hit in response.Hits)
                {
                    richTextBox1.AppendText("Name: " + hit.Source.name.ToString()
                    + Environment.NewLine
                    + "Original_voice_actor: " + hit.Source.original_voice_actor.ToString()
                    + Environment.NewLine
                    + "Animated_debut: " + hit.Source.animated_debut.ToString()
                    + Environment.NewLine
                    + Environment.NewLine);
                }
            }
        }       
    }
}
